package com.cdr.android.whereishank;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

/*
 * Settings activity handles everything that happens on the settings page
 * UI updates/responses to clicks
 * saving Prefs entered on this page
 */
public class SettingsActivity extends Activity {
    public static final String PREFS_NAME = "whereishankPrefsFile";
    static final public String RESULT = "com.cdrsoft.whereishank.SettingsActivity.REQUEST_PROCESSED"; // String that is attatched to Intent broadcasted to update UI on main thread, Receiver on main thread only receives intents with this string
	private static final String DEBUGMESSAGES = "DEBUG_MESSAGES";
    static String MESSAGE = "UIUPDATE";//Message sent with intent sent to mainthread similar to String Result 
    LocalBroadcastManager broadcaster; //broadcasts intent to main thread to update UI, gets initiated onStart of this and used in the sendMainUiMethod called after url is sent
    EditText mEditText, passwordEditText, mGroupEditText;
	SeekBar intervalBar;
	LinearLayout buttonLayout;
	Button setHomeButton;
	TextView passwordTextView, intervalTextView;
	private CheckBox debugCheckBox;

	
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitymenu);//sets view
        
        broadcaster = LocalBroadcastManager.getInstance(this);

        // Make sure we're running on Honeycomb or higher to use ActionBar APIs
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
        

        LinearLayout buttonLayout = (LinearLayout) findViewById(R.id.settingsBackGround);        
        buttonLayout.setOnLongClickListener(new OnLongClickListener() {

            private boolean mPasswordVisability;

			@Override
			/*
			 * (non-Javadoc)
			 * @see android.view.View.OnLongClickListener#onLongClick(android.view.View)
			 * handles user long click to show edittext to enter password
			 */
            public boolean onLongClick(View v) {
            	int vs;
            	//togle vissability everytime user long clicks so that edittext isnt stuck on
            	mPasswordVisability = !mPasswordVisability;
            	if(mPasswordVisability){
            		vs = 0;
            	} else {
            		vs = 8;
            	}
                passwordTextView.setVisibility(vs);
                passwordEditText.setVisibility(vs);
                
                PasswordTextListener pListener = new PasswordTextListener();
                passwordEditText.setOnKeyListener(pListener);
                 
                SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                passwordEditText.setText(settings.getString("password", null));
                  
                sendMainUiBroadcast("upDate");
                  
                return true;
            }

			class PasswordTextListener implements OnKeyListener{
				
				final String correctPassword = "1961";
			
				@Override
					public boolean onKey(View V, int keyCode, KeyEvent event) {
						if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)){
							
							SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
						    SharedPreferences.Editor editor = settings.edit();	
						    
						    String p = passwordEditText.getText().toString();
						    int veiwMode;
						    
							if (p.equals(correctPassword)){
						    	veiwMode = 0;
						    } else {
						    	veiwMode = 8;
						    }
											
							editor.putInt("veiwMode", veiwMode).commit();
							
							displayHomeButtonCheck();
						}				
						return false;
					}
				  
			  }

			private void sendMainUiBroadcast(String message) {
				Intent intent = new Intent(RESULT);
				if(message != null){
					intent.putExtra(MESSAGE, message);
				}
				
				broadcaster.sendBroadcast(intent);
			}

        });
        
        mEditText = (EditText) findViewById(R.id.editNameText);
        EditTextListener aListener = new EditTextListener(); //edittextListener is my own class below
        mEditText.setOnKeyListener(aListener);
        
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        mEditText.setText(settings.getString("name", "Name"));
        
        passwordTextView = (TextView) findViewById(R.id.passwordTextView);
        passwordTextView.setVisibility(View.GONE);

		intervalTextView = (TextView) findViewById(R.id.updateIntervalText);
		int currentseekBarProgress = settings.getInt("SeekBarProgress", 0);
		intervalTextView.setText("Update Interval: " + String.valueOf(((currentseekBarProgress+1)*5)) + " minutes");
        
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        passwordEditText.setVisibility(View.GONE);
        
        intervalBar = (SeekBar) findViewById(R.id.interval_bar);        
        intervalBar.setProgress(currentseekBarProgress);
        intervalBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            int updateInterval = 0;
 
            @Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
            	updateInterval = ((progress+1)*5);

				intervalTextView.setText("Update Interval: " + String.valueOf(updateInterval) + " minutes");
                
                SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        	    SharedPreferences.Editor editor = settings.edit();
        	    editor.putInt("UpdateInterval", updateInterval).commit();
        	    editor.putInt("SeekBarProgress", progress).commit();
            }
 
            @Override
			public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
 
            @Override
			public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        
        displayHomeButtonCheck();

    }

    private void displayHomeButtonCheck() { 
    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    	int vs = settings.getInt("veiwMode", 8);
    	
        setHomeButton = (Button) findViewById(R.id.setashomebutton);		
		setHomeButton.setVisibility(vs);
	}

	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    public void setAsHome(View view) {
    	
    	Intent mServiceIntent = new Intent(this.getApplicationContext(),UpdateService.class); //create intent to start UpdateService
		startService(mServiceIntent);//starts service using mServiceIntent
    	
    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
	    SharedPreferences.Editor editor = settings.edit();
	    editor.putString("homeLatString", settings.getString("latString", null)).commit();
	    editor.putString("homeLongString", settings.getString("longString", null)).commit();
	    editor.putString("homeCityString", settings.getString("cityString", null)).commit();
    	editor.putString("homeTzString", settings.getString("tzString", null)).commit();
    	
		}
    
  private class EditTextListener implements OnKeyListener{

		@Override
		public boolean onKey(View V, int keyCode, KeyEvent event) {
			if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)){
				
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		    SharedPreferences.Editor editor = settings.edit();	
		    
		    editor.putString("name", mEditText.getText().toString()).commit();
				
			}				
			return false;
		}
    	
    }

}