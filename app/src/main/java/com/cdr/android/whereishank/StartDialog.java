package com.cdr.android.whereishank;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class StartDialog extends DialogFragment {
	
	protected static final String TAG = "StartDialog";
	protected static final String PREFS_NAME = "whereishankPrefsFile";
	private EditText nameEditText, groupEditText;
	protected boolean mGroupChanged = false;
	protected boolean mNameChanged = false;
	private AlertDialog dialog;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		View v = getActivity().getLayoutInflater().inflate(R.layout.dialog_namegroup, null);
		groupEditText = (EditText)v.findViewById(R.id.dialoggroup);
		nameEditText = (EditText)v.findViewById(R.id.dialogname);
		this.setCancelable(false);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		dialog =  builder
						.setView(v)
						.setTitle(R.string.start_dialog_tittle)
						.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {									
							@Override
							public void onClick(DialogInterface dialog, int which) {
								Log.d(TAG, "hello: " + groupEditText.getText().toString() + 
										nameEditText.getText().toString());
								SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
							    SharedPreferences.Editor editor = settings.edit();	
							    
							    editor.putString("name", nameEditText.getText().toString()).commit();
								editor.putString("group", groupEditText.getText().toString()).commit();
							}
						})
						.show();
		
		dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
		dialog.setCanceledOnTouchOutside(false);
		
		nameEditText.addTextChangedListener(new TextWatcher(){

			@Override
			public void afterTextChanged(Editable s) {
				mNameChanged  = true;
				addPositiveButton();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		groupEditText.addTextChangedListener(new TextWatcher(){

			@Override
			public void afterTextChanged(Editable arg0) {
				mGroupChanged  = true;
				addPositiveButton();
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
		});
								
		return dialog;
	}

	protected void addPositiveButton() {
		if ((mGroupChanged == true) && (mNameChanged == true) && (groupEditText.length() > 0) && (nameEditText.length() > 0)){
			dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(true);
		} else {
			dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
		}
	}
	
}
