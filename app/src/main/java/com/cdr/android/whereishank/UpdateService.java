package com.cdr.android.whereishank;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.TimeZone;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.Time;
import android.util.Log;

/*
 * Udpdate service runs in background even if app is killed
 * 
 * finds location from gps or network with lm and ll
 * finds name of location from MyReverseGeocoder
 * sends url to update server using ServiceHttpGetter
 * 
 */
public class UpdateService extends Service {// Service runs in background finding location from gps and sending it to server then saving what it sends in preferences
	
	  	private static final String TAG = "com.cdrsoft.whereishank";//debuging tag
	  	public static final String PREFS_NAME = "whereishankPrefsFile"; //name of file where preferences (variables, names, locations, etc are saved
		static final public String RESULT = "com.cdrsoft.whereishank.UpdateService.REQUEST_PROCESSED"; // String that is attatched to Intent broadcasted to update UI on main thread, Receiver on main thread only receives intents with this string
		private static final String GROUP = "group";
		public static final String ACCCURACY = "accuracy";
		static String MESSAGE = "UIUPDATE";//Message sent with intent sent to mainthread similar to String Result 

		
		LocationManager lm;//Location Manager used to receive gps location
		myLocationListener ll;//Location Listener used to get gps location. an extended class of LocL
		
		int buttonState = 3;//what button is depressed, 0 is tracking, 1 is put me home, 2 is keep me here, 3 is disable tracking, disable tracking is default
		
		String url; //the url that gets built to sent to the server to display info on the web
		
		LocalBroadcastManager broadcaster; //broadcasts intent to main thread to update UI, gets initiated onStart of this and used in the sendMainUiMethod called after url is sent
		private boolean gps_enabled;
		private boolean network_enabled;
		
		Location loc;
		private Runnable runnable;
		private PendingIntent pendingIntent;
		private Handler handler;
		
		public UpdateService(){
			super();
			broadcaster = null;
			lm = null;
			ll = null;
			pendingIntent = null;
			handler = null;
			runnable = null;
		  }		
		
	    @Override
	    /*
	     * (non-Javadoc)
	     * @see android.app.Service#onStartCommand(android.content.Intent, int, int)
	     * does the work of Update service
	     */
	    public int onStartCommand(Intent intent, int flags, int startId) { //when sevice starts
	    	
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

	        Log.d("LocalService", "Received start id " + startId + ": " + intent);//log in debug that service started
	        
	        /*
	         * do creations in if statement incase they exist from
	         * last time Update service was created
	         */
	        
	        if (broadcaster == null){
	        	handler = new Handler();
				broadcaster = LocalBroadcastManager.getInstance(this); //set up broadcaster to be used later 
				lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);//setup manager to get location
				ll = new myLocationListener();//setup listener to receive location from LOC_SERVICE
				//runnable is the instructions for what the handler will do to turn off gps
				runnable = new Runnable() {
					   @Override
					   public void run() {
						   lm.removeUpdates(ll);
					   }
					};
	        }
	        
	        scheduleNextUpdate();//method shcedulenextupdate sets an alarm for the next time UpdateService is called
			
	        //TODO make this simple by using buttonstate directly instead of following, homeing, keeping, nttracking
			if (settings.getBoolean("following", false) == true) buttonState = 0; // \
			if (settings.getBoolean("homeing", false) == true) buttonState = 1;   //  \ __set button state based on what button was pressed last and stored in shared preferences
			if (settings.getBoolean("keeping", false) == true) buttonState = 2;   //   /
			if (settings.getBoolean("nottracking", false) == true) buttonState = 3;// /

		   
		    
			//exceptions will be thrown if provider is not permitted.
			/*
			 * note that network enabled is not access to the network but ability to use
			 * network to find location
			 */
		
	        try{gps_enabled=lm.isProviderEnabled(LocationManager.GPS_PROVIDER);}catch(Exception ex){}
	        try{network_enabled=lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);}catch(Exception ex){}
	        
	        //don't start listeners if no provider is enabled
	        if(gps_enabled || network_enabled){
	        		findHere(); // method used to get location from gps
	        	}
		    
		    return START_STICKY; // We want this service to continue running until it is explicitly stopped, so return sticky
	        
	    }
	    
		@Override
		/*
		 * (non-Javadoc)
		 * @see android.app.Service#onBind(android.content.Intent)
		 */
		public IBinder onBind(Intent intent) {
			// TODO Auto-generated method stub
			return null;
		}
		
	    
	    

	  private void scheduleNextUpdate() {//sets up a time when UpdateService will start again called in onCreate
		
		 AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE); //initiallize alarmManager as an AlarmManager from Context
		 
		  
		if (pendingIntent != null){
			alarmManager.cancel(pendingIntent);
			pendingIntent = null;
		}
		  
	    Intent intent = new Intent(this, this.getClass());//creates an Intent called "intent" that starts this class (UpdateService)
	    pendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT); // takes intent and sets a pendingintent for this class

	    long currentTimeMillis = System.currentTimeMillis();//sets variable currentTimeMillis to the timestamp for when service was ran
	    
	    //get update interval user entered in settings menue
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        long updateInterval = settings.getInt("UpdateInterval", 5);
        if (updateInterval < 5) {
        	updateInterval = 5;
        }
        
        if (updateInterval > 60){
        	updateInterval = 60;
        }
        
	    long nextUpdateTimeMillis = currentTimeMillis + (updateInterval*60*1000);//adds a time interval to current time
	    Time nextUpdateTime = new Time(); //create a new Time instance called nextUpdate time
	    //QUESTION // nextUpdateTime isnt used
	    nextUpdateTime.set(nextUpdateTimeMillis); //sets variable nextUpdate to the value of nextUpdateTimeMillis

	    alarmManager.set(AlarmManager.RTC, nextUpdateTimeMillis, pendingIntent); //sets an alarm for time created above
	    
	  }
	  
	  private void findHere() { // finds location from gps and starts server update. called in onCreate
		  	if (buttonState != 0){
		  		lm.removeUpdates(ll);
		  		updateServer();
		  		
		  		return;
		  	}
			Criteria criteria = new Criteria(); //create a new Criteria instance called Criteria used to determine how to find location
			//TODO I think you can add response time to qive up here
			criteria.setAccuracy(Criteria.ACCURACY_MEDIUM); // sets accuracy for criteria as low ~500m
			criteria.setPowerRequirement(Criteria.POWER_LOW); //sets power for criteria as low
			
			//handler is delayed to turn off gps after the second param, (in millis)	
			handler.postDelayed(runnable, 90*1000);
			
			String prov = lm.getBestProvider(criteria, true);
			/*
			 * this is where gps gets turned on. 
			 *when gps gets a fix myLocationListener.onLocationChange() will be called
			 *myLocation Listener creates MyGeoCodingTask and executes it onLocationChange
			 *onLocationChange also calls upDateServer()	
			 */
			
			lm.requestSingleUpdate(prov, ll, null);//get a single response from location manager lm using location listener ll			
		}
				
		private void updateServer() {
			
			if (buttonState == 0) {//Tracking
				sendUrlRequest(0);
			}
			if (buttonState == 1) {//keep home
				sendUrlRequest(1);
			}
			if (buttonState == 2){//keep me here
				sendUrlRequest(2);
			}
			if (buttonState == 3) {//Disable Tracking
				return;
			}
		}
		
		private boolean isOnline() {
			ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		    
			return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
		}

		private void sendUrlRequest(int bs) {
			String latString = null;
			String longString = null;
			String cityString = null;
			String name = null;
			String group = null;
			String tz = null;
			long updateTime = 0;
			
			
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
			 
			if (bs == 0){ //Tracking
				latString = settings.getString("latString", null);
				longString = settings.getString("longString", null);
				cityString = settings.getString("cityString", null);
				updateTime = settings.getLong("lastupdatetime", 0);
	            TimeZone z = TimeZone.getDefault();//set z to timezone of phone 
	            tz = z.getDisplayName(false, TimeZone.SHORT);//set tz to String short timezone (e.g. "PST")
			}
			
			if (bs == 1){//keep home
				latString = settings.getString("homeLatString", "39.1000");
				longString = settings.getString("homeLongString", "-84.5167");
				cityString = settings.getString("homeCityString", "Cincinnati, OH");
				updateTime = settings.getLong("lastupdatetime", System.currentTimeMillis()/1000);
				tz = settings.getString("homeTzString", null);
			}
			
			if (bs == 2){//Keep Me Here
				latString = settings.getString("keepLat", "39.1000");
				longString = settings.getString("keepLong", "-84.5167");
				cityString = settings.getString("keepCity", "Cincinnati, OH");
				updateTime = settings.getLong("lastupdatetime", 0);
	            tz = settings.getString("keepTzString", null);
			}
			
			if (buttonState == 3) {//if tracking is off dont do anything
				return;
			}
				
			 
			name = settings.getString("name", null);//set name to user's name, default to "Hank"
			group = settings.getString(GROUP, null);
			
			 if ((latString == null) || (longString == null) || (cityString == null) 
					 || (tz == null) || (name == null) || (group == null)) {
				 return;
			 }
					 	
			try {
                String domain = getResources().getString(R.string.tracking_domain);
				url = "http://" + domain + "/WhereIsHank/track.php?"
								+"lat="+ URLEncoder.encode(latString, "ISO-8859-1")
								+"&long="+ URLEncoder.encode(longString, "ISO-8859-1")
								+"&updatetime="+ URLEncoder.encode(Long.toString(updateTime), "ISO-8859-1")
								+"&label="+ URLEncoder.encode(name, "ISO-8859-1")
								+"&group="+ URLEncoder.encode(group, "ISO-8859-1")
								+"&tz="+ URLEncoder.encode(tz, "ISO-8859-1")
								+"&city="+ URLEncoder.encode(cityString, "ISO-8859-1");//build url string
				
				SharedPreferences.Editor editor = settings.edit();
				editor.putString("lastsentLat", latString).commit();
				editor.putString("lastsentlong", longString).commit();
				editor.putString("lastsentCity", cityString).commit();
				editor.putLong("lastsentTime", System.currentTimeMillis()/1000).commit();
				editor.putString("lastsentTZ", tz).commit();
				
				sendMainUiBroadcast("upDate");
				
				
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}	
			
			///would be send url method seperate from build url
			
			try {
				ServiceHttpGetter get = new ServiceHttpGetter();
				URL ulr;
				ulr = new URL(url);
				get.execute(ulr); 
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		
		}
		
private void sendMainUiBroadcast(String message) {
			Intent intent = new Intent(RESULT);
			if(message != null){
				intent.putExtra(MESSAGE, message);
			}
			this.sendBroadcast(intent);
		}

class ServiceHttpGetter extends HttpGetter{
	
	@Override
	protected void onPostExecute(String result) {
	}
		        
}

class myLocationListener implements LocationListener{
		
	class MyReverseGeocodingTask extends ReverseGeocodingTask{
		
		public MyReverseGeocodingTask(Context context) {
			super(context);
		}

		@Override
		protected void onPostExecute(String result) {
			if (result != null){
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
			    SharedPreferences.Editor editor = settings.edit();
			    
			    editor.putString("cityString", result).commit();
			    
			    TimeZone z = TimeZone.getDefault();//set z to timezone of phone 
	            String tz = z.getDisplayName(false, TimeZone.SHORT);
			    editor.putString("tzString", tz).commit();
			}
	        
	    }
		
	}
	
	@Override
	public void onLocationChanged(Location location) {	
		if (location != null){
			double pLat = location.getLatitude();
			double pLong = location.getLongitude();
			float pAccuracy = location.getAccuracy();
			
			if (pAccuracy > 5000) {
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = settings.edit();
			    editor.putString(ACCCURACY, Float.toString(pAccuracy)).commit();
			    sendMainUiBroadcast("upDate");
				return;
			}
			
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		    SharedPreferences.Editor editor = settings.edit();
		    
		    editor.putString(ACCCURACY, Float.toString(pAccuracy)).commit();
		    editor.putString("latString", Double.toString(pLat)).commit();
		    editor.putString("longString", Double.toString(pLong)).commit();
		    editor.putLong("lastupdatetime", System.currentTimeMillis()/1000).commit();
		    			
		    
		    
		    lm.removeUpdates(ll);
		    handler.removeCallbacks(runnable);
		    
		    MyReverseGeocodingTask get = new MyReverseGeocodingTask(getApplicationContext());
			loc = new Location(location);
			get.execute(loc);
		    
		 
	    	updateServer();
	    	Log.d(TAG, "Tracking");
		    
		    
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		lm.removeUpdates(ll);
		handler.removeCallbacks(runnable);
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		if (status == 0){
			lm.removeUpdates(ll);
			handler.removeCallbacks(runnable);
		}
	}
			
}
			
}
		
