package com.cdr.android.whereishank;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


/*
 * AutoStart starts when the phone sends broadcast that boot is completed
 * and starts the UpdateService
 * 
 * the receiver that gets created and registered in the manifest
 * in the receiver tag
 */
public class AutoStart extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		
		 Intent i = new Intent(context,UpdateService.class);
		 context.startService(i);
	}

}

