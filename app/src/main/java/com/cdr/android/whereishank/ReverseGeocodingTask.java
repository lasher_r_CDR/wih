package com.cdr.android.whereishank;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;

/*
 * ReverseGeocoding gets location names from Google using a Location
 * 
 * because sending the request can take some time it's an AsyncTask.
 * onPostExecute is overridden when ReverseGeocoding gets created.
 */
class ReverseGeocodingTask extends AsyncTask<Location, Void, String> {
	Context mContext;

    public ReverseGeocodingTask(Context context) {
        super();
        mContext = context;
    }

    @Override
    protected String doInBackground(Location... params) {
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());

        Location loc = params[0];
        List<Address> addresses = null;
        try {
            // Call the synchronous getFromLocation() method by passing in the lat/long values.
            addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            Address address = addresses.get(0);
            // Format the city, and country name.
            String addressText = String.format("%s, %s" , 
                    address.getLocality(),
                    address.getAdminArea());
            
            return addressText;
        }
        return null;
    }
    
    @Override
	protected void onPostExecute(String result) {
    	
    }
    
}
