package com.cdr.android.whereishank;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.ToggleButton;


/*
 * main activity, this is what is shown when the app starts.  It shows the main UI, allows access to the settings, 
 * starts the first use dialog, and starts the update service
 */
public class MainActivity extends FragmentActivity {
    public static final String PREFS_NAME = "whereishankPrefsFile"; //the name of the location where preferences (like button state, lat long, name, etc) are stored
    int buttonState;// what button was depressed last. 0 = tracking, 1= keep home, 2 = keep here, 3 = not tracking
	//TODO get rid of the next 4
    boolean following;
	boolean homeing;
	boolean keeping;
	boolean nottracking;
	ToggleButton followbutton, puthomebutton, keepherebutton, disablebutton;// buttons on main ui
	private BroadcastReceiver receiver;//broadcast receiver used to receive broadcasts from UpdateService to update UI
	TextView mAddressText, mTimeText, mAccuracyText;//TextVeiws of the main UI
	private static final String DIALOG_START = "start";
	private static final String GROUP = "group";
	private static final String ACCURACY = "accuracy";

	@Override
	protected void onStart() {
	    super.onStart();
	    LocalBroadcastManager.getInstance(this).registerReceiver((receiver), new IntentFilter(UpdateService.RESULT));//register reciever here instead of in the manifest
	    upDateUI();
	}

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 * onCreate, does all the work of MainActivity.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activitymain);//sets the view for the main UI to activity_main.xm
		
		upDateUI();//update UI on create
		
		loadButtons(); //Loads buttonStates out of prefferences
		
		startDialogCheck();//checks if name and group exist and starts dialog if they dont
		
		startUpdateService(); //Method used to start UpdateService
	}

    @Override
    protected void onResume(){
        super.onResume();

        IntentFilter filter = new IntentFilter(); //creates instance of IntentFilter called filter used to reseive UI updates from UpdateService
        filter.addAction("SOME_ACTION");
        filter.addAction("SOME_OTHER_ACTION");
        /*
		 * Because Update Service cannot update UI it sends a broadcast out and the mainthread catches it
		 * with this receiver
		 */
        receiver = new BroadcastReceiver() { //receiver is made an instance of broadcastReceiver
            @Override
            public void onReceive(Context context, Intent intent) {//receiver onReceive() is overridden
                Log.d("com.cdrsoft.whereishank", "Receiver worked");//logs that receiver received an intent

                upDateUI();//method used to update UI when intent is received
            }
        };

        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(receiver, filter); //registers receiver instead of in the manifest
        Log.d("hello", "hello");
    }

    @Override
    protected void onPause(){
        super.onPause();
        try {
            if (receiver!=null) {
                LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(receiver);
                receiver=null;
            }//unregister receiver
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
	
	private void upDateUI() {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
	
	    mAddressText = (TextView)findViewById(R.id.cityTextView);
	    mAddressText.setText(settings.getString("lastsentCity", ""));
	    
	    
	    
	    mTimeText = (TextView)findViewById(R.id.timeTextView);
	    Date date = new Date(settings.getLong("lastsentTime", 0)*1000L);
	    SimpleDateFormat sdf = new SimpleDateFormat("E MM/dd 'at' hh:mm a zzz", Locale.US);
	    String formatedDate = sdf.format(date);
	    mTimeText.setText(formatedDate);
	    
	    mAccuracyText = (TextView)findViewById(R.id.accuracyTextView);
	    if (settings.getString(ACCURACY, "").equals("")){
		    mAccuracyText.setText("Unable to retrieve GPS location.");
	    } else
	    	mAccuracyText.setText("Accuracy to within: " + settings.getString(ACCURACY, "") + " meters");
	
	    
		//wire buttons to view
		followbutton = (ToggleButton)findViewById(R.id.follow_button);   // \
		puthomebutton = (ToggleButton)findViewById(R.id.home_button);    //  \__sets buttons to UI resorces
		keepherebutton = (ToggleButton)findViewById(R.id.here_button);   //  /
		disablebutton = (ToggleButton)findViewById(R.id.disable_button); // /
		
		
		int vs = settings.getInt("veiwMode", 8);
		
		puthomebutton.setVisibility(vs);
		keepherebutton.setVisibility(vs);
		
	}

	private void loadButtons() {//method used to update buttonstates
		//TODO major overhaul this method
		//load preferences
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		
		 //get following button state when app was quit from preferences, return false by default 
		following = settings.getBoolean("following", false);
		 
		//if following button was pressed on quit, set followbutton as on and set button state
		if (following == true) 
		{followbutton.setChecked(true);
		buttonState = 0;}
		
		 //get homeing button state when app was quit from preferences, return false by default 
		homeing = settings.getBoolean("homeing", false);		
		
		//if homeing button was pressed on quit, set homeing button as on and set button state
		if (homeing == true) 
		{puthomebutton.setChecked(true);
		buttonState = 1;}
		
		 //get following keeping state when app was quit from preferences, return false by default 
		keeping = settings.getBoolean("keeping", false);
		
		//if keeping button was pressed on quit, set keeping button as on and set button state
		if (keeping == true) 
		{keepherebutton.setChecked(true);
		buttonState = 2;}
		
		 //get not tracking button state when app was quit from preferences, return false by default 
		nottracking = settings.getBoolean("nottracking", false);
		
		//if nottracking button was pressed on quit, set not tracking button as on and set button state
		if (nottracking == true) 
		{disablebutton.setChecked(true);
		buttonState = 3;}
		
	}

	private void startDialogCheck() {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		if ((settings.getString(GROUP, null) == null ) || (settings.getString("name", null) == null)){
			FragmentManager fm = getSupportFragmentManager();
			StartDialog dialog = new StartDialog();
			dialog.show(fm, DIALOG_START);
		}
	}


	private void startUpdateService() { //Creates a new Intent to start UpdateService
		
		Intent mServiceIntent = new Intent(this.getApplicationContext(),UpdateService.class); //create intent to start UpdateService
		startService(mServiceIntent);//starts service using mServiceIntent
		
	} 
		
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {//creates options menue
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);//sets menu view to menu.main.xml
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) { //method for when menu item is clicked
		
		Intent intent = new Intent(this, SettingsActivity.class); //create intent to start SettingsActivity
		startActivity(intent);//start activity using intent
		
		return false;
	}
	

	@Override
	protected void onStop(){
		//save button states when app is quit
		saveButtonStates();
        super.onStop();
	}

	public void followButton(View view){//when followbutton is pressed
		followbutton.setChecked(true);//turn on follow button
	    puthomebutton.setChecked(false);// \
	    keepherebutton.setChecked(false);// \
		disablebutton.setChecked(false);//   \____turn off all other buttons, makes buttons behave like radio buttons
		buttonState = 0;//set buttonState
		saveButtonStates();// save button state
		startUpdateService(); //Method used to start UpdateService
	}
	
	/*
	 * the next for methods are called when their respective buttons are pressed
	 * methods are assigned to each button in main's layout xml file
	 * with tag android:onClick
	 * all buttons follow the pattern of follow button
	 */
	
	public void putMeHomeButton(View view){
		followbutton.setChecked(false);
	    puthomebutton.setChecked(true);
	    keepherebutton.setChecked(false);
		disablebutton.setChecked(false);
		saveButtonStates();
		buttonState = 1;
		startUpdateService();
	}
	
	public void keepMeHereButton(View view){
		followbutton.setChecked(false);
	    puthomebutton.setChecked(false);
	    keepherebutton.setChecked(true);
		disablebutton.setChecked(false);
		saveButtonStates();
		buttonState = 2;
		 
		/*
		 * keepmehere is a little different after main button pattern
		 */
		
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); //get access to PREFS_NAME from shared preferences instance settings
		 
		String keepLat = settings.getString("latString", null);//get  latString (last lat from gps) out of settings
		String keepLong = settings.getString("longString", null);//same for long
		String keepCity = settings.getString("cityString", null);//same for city name
		String keepTz = settings.getString("tzString", null);//and timezone
		
		 
		
	    SharedPreferences.Editor editor = settings.edit(); //create editor to put values int preferences
	    
	    editor.putString("keepLat", keepLat).commit();//put  values of above into preferences
	    editor.putString("keepLong", keepLong).commit();
	    editor.putString("keepCity", keepCity).commit();
	    editor.putString("keepTzString", keepTz).commit();
	    
		startUpdateService(); //Method used to start UpdateService

	    
	}
	
	/*
	 * return to main button pattern
	 */
	
	public void disableTrackingButton(View view){
		followbutton.setChecked(false);
	    puthomebutton.setChecked(false);
	    keepherebutton.setChecked(false);
		disablebutton.setChecked(true);
		saveButtonStates();
		buttonState = 3;
	}
	
	private void saveButtonStates() {//saves button state
		//TODO make this use button state
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	    SharedPreferences.Editor editor = settings.edit();
	    editor.putBoolean("following", followbutton.isChecked()).commit();
	    editor.putBoolean("homeing", puthomebutton.isChecked()).commit();
	    editor.putBoolean("keeping", keepherebutton.isChecked()).commit();
	    editor.putBoolean("nottracking", disablebutton.isChecked()).commit();
	    		
	}
		   	 	
}
