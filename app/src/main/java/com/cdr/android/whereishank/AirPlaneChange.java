package com.cdr.android.whereishank;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;

/*
 * AirplaneChange starts when the phone sends broadcast that airplane mode is toggled
 * because the broadcast is sent everytime airplane mode changes this class checks
 * if it was toggled to off and starts the UpdateService
 * 
 * the receiver that gets created and registered in the manifest
 * in the receiver tag
 */
public class AirPlaneChange extends BroadcastReceiver {
	   @Override
	public void onReceive(Context context, Intent intent) {
		   
		   boolean airPlane = isAirplaneModeOn(context);
		   
		   if (airPlane) {
			   Intent i = new Intent(context,UpdateService.class);
			   context.startService(i);
		   }
	   }    
	   
	   @SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	   public static boolean isAirplaneModeOn(Context context) {

		    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
		        return Settings.System.getInt(context.getContentResolver(), 
		                Settings.System.AIRPLANE_MODE_ON, 0) != 0;          
		    } else {
		        return Settings.Global.getInt(context.getContentResolver(), 
		                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
		    }       
		}
}