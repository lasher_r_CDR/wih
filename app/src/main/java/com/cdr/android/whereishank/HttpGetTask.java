package com.cdr.android.whereishank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

/*
 * HTTPGetter does the work of sending the url that is passed to it to
 * the server
 * 
 * because sending the url can take some time it's an AsyncTask.
 * onPostExecute is overridden when HttoGetter gets created.
 */
class HttpGetter extends AsyncTask<URL, Void, String> {

    static String time;
	public int done = 0;

	@Override
    protected String doInBackground(URL... urls) {
            StringBuilder builder = new StringBuilder();
            HttpClient client = new DefaultHttpClient();
            String url = urls[0].toString();
            HttpGet httpGet = new HttpGet(url);
           
            
            try {
                    HttpResponse response = client.execute(httpGet);
                    StatusLine statusLine = response.getStatusLine();
                    int statusCode = statusLine.getStatusCode();
                    if (statusCode == 200) {
                            HttpEntity entity = response.getEntity();
                            InputStream content = entity.getContent();
                            BufferedReader reader = new BufferedReader(
                                            new InputStreamReader(content));
                            String line;
                            while ((line = reader.readLine()) != null) {
                                    builder.append(line);
                            }
                            Log.v("Getter", "Your data: " + builder.toString()); //response data
                            done  = 1;
                            SimpleDateFormat sdfDate = new SimpleDateFormat("E MM/dd 'at' hh:mm a zzz", Locale.US);
                            Date now = new Date();
                            String strDate = sdfDate.format(now);
                            return strDate;

                    } else {
                            Log.e("Getter", "Failed to download file");
                            
                    }
            } catch (ClientProtocolException e) {
                    e.printStackTrace();
            } catch (IOException e) {
                    e.printStackTrace();
            }
         
      return null;      
    }

    
    
}